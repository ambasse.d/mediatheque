class Imprimable:
    def __init__(self, titre):
        self.titre = titre

    def __str__(self):
        return str("L’impression du document " + self.title + " est lancée ")


class DocsTexte(Imprimable):
    """ Implémentation de l'objet DocsTexte : Documents Texte."""

    def __init__(self,  titre,auteur, copyright):
        self.title = titre
        self.auteur = auteur
        self.copyright = copyright

    def getCopyright(self):
        return str("@" + self.copyright)

    def getTitle(self):
        return str(self.title)

    def getAuteur(self):
        return str(self.auteur)

    def __str__(self):
        return str(self.title, "est écrit par", self.auteur)


class Article(DocsTexte, Imprimable):

    def __init__(self, titreRevue, auteur,  copyright, editeur, dateParution, numEdition):

        """# Element d'un document text
        self.auteur = auteur
        self.title = titreRevue
        self.copyright = copyright
        # Element d'un document Article
        self.dateParution = dateParution
        self.editeur = editeur
        self.numEdition = numEdition
"""
    def getEditeur(self):
        return str(self.editeur)

    def getNumEdition(self):
        return str(self.numEdition)

    def __str__(self):
        return str("Article publié dans " + self.title + " , numéro: " + self.numEdition)


class Livre(DocsTexte,Imprimable):

    def __init__(self,titre, auteur,anneeParution,editeur):
        # Element d'un document texte
        self.auteur = auteur
        self.titre = titre
        self.copyright = copyright
        # Element d'un document Livre
        self.anneeParution = anneeParution
        self.editeur = editeur

    def getEditeur(self):
        return str(self.editeur)

    def getAnneeParution(self):
        return self.anneeParution

    def __str__(self):
        return str(
            "Ce livre est écrit par " + self.auteur + " intitulé " + self.title + "sorti en " + self.anneeParution)


class DocsAudio:
    """ Implémentation de l'objet DocsAudio : Documents Audio."""

    def __init__(self, auteur, title):
        self.auteur = auteur
        self.titre = title


class DocsVideo:
    """ Implémentation de l'objet DocsVideo : Documents Video."""

    def __init__(self, auteur, titre):
        self.auteur = auteur
        self.titre = titre


    def getTitre(self):
        return self.titre

    def getAuteur(self):
        return self.auteur

    def __str__(self):
        return str("Cette vidéo est crée par" + self.auteur + " intitulé " + self.titre)


class DocsMultiMedia:
    """ Implémentation de l'objet DocsMultimedia : Documents Multimedia."""

    def __init__(self, auteur, titre):
        self.titre = titre
        self.auteur = auteur

    def getTitre(self):
        return self.titre

    def getAuteur(self):
        return self.auteur

    def __str__(self):
        return str("Document multimédia :" + self.titre)


class Mediatheque():
    """ Implémentation d'une Mediatheque."""

    def __init__(self):
        self.__corps = []
        self.video = DocsVideo()

    def __str__(self):
        res = ""
        for doc in self.__corps:
            res += doc
        return res

    def addArticle(self, titreRevue, copyright, auteur, editeur, dateParution, numEdition):
        newArticle = Article(titreRevue, copyright, auteur, editeur, dateParution, numEdition)

        self.__corps.append(newArticle)

    def addVideo(self, auteur, titre):
        newVideo =
        self.__corps.append(newVideo)

    def addLivre(self, titre,auteur,  copriright, editeur, anneeParution):
        newLivre = Livre(auteur, titre, copriright, editeur, anneeParution)
        self.__corps.append(newLivre)

    def addDocsAudio(self, auteur, titre):
        newDocsAudio = DocsAudio(auteur, titre)
        self.__corps.append(newDocsAudio)

    def addDocsMultimedia(self, auteur, titre):
        newDocsMultimedia = (titre,auteur)
        self.__corps.append(newDocsMultimedia)

if __name__ == "__main__":
    maMediatheque = Mediatheque()
    print(maMediatheque)
